#include <stddef.h>

/*$
 * requires: valid_bytes(buffer, buffersize);
 * assigns: buffer[0,buffersize);
 * ensures: valid_primed_substring(buffer,buffersize);
 * ensures: return <= buffersize;
 */
size_t quotearg_buffer_restyled (char *buffer, size_t buffersize,
                          char const *arg, size_t argsize,
                          enum quoting_style quoting_style, int flags,
                          unsigned int const *quote_these_too,
                          char const *left_quote,
                          char const *right_quote);
