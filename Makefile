##############################################################################
#                                                                            #
#  This file is part of MOPSA, a Modular Open Platform for Static Analysis.  #
#                                                                            #
#  Copyright (C) 2017-2019 The MOPSA Project.                                #
#                                                                            #
#  This program is free software: you can redistribute it and/or modify      #
#  it under the terms of the GNU Lesser General Public License as published  #
#  by the Free Software Foundation, either version 3 of the License, or      #
#  (at your option) any later version.                                       #
#                                                                            #
#  This program is distributed in the hope that it will be useful,           #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#  GNU Lesser General Public License for more details.                       #
#                                                                            #
#  You should have received a copy of the GNU Lesser General Public License  #
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.     #
#                                                                            #
##############################################################################

# Makefile to run Mopsa on Coreutils programs
##


##############
# Parameters #
##############

# Root directory of mopsa
ifneq ($(MOPSA_ROOT),)
MOPSA_BIN := $(realpath $(MOPSA_ROOT))/bin/
endif

# Root directory of coreutils sources
SRC ?= src/coreutils-8.30

# Analysis timeout in seconds
TIMEOUT ?= 3600

# List of programs to analyze
PROGRAMS ?= $(shell cat support/programs.txt)

# Analysis scenarios determine how the program is called
SCENARIOS ?= no-arg one-symbolic-arg many-symbolic-args

# Analysis abstractions
ABSTRACTIONS ?= a1 a2 a3 a4

# Directory containing the results
OUTPUT ?= results


#############
# Constants #
#############

CASES := \
	$(foreach program,${PROGRAMS},\
	$(foreach scenario,${SCENARIOS},\
	$(foreach abstraction,${ABSTRACTIONS},\
	${program}/${scenario}/${abstraction})))

# Mopsa analyzer binary
MOPSA := ${MOPSA_BIN}mopsa-c

# Mopsa wrapper for make
MOPSA_MAKE := ${MOPSA_BIN}mopsa-build make

# Flags of the script support/analyze.py
FLAGS = --mopsa=${MOPSA}

ifneq ($(VERBOSE),)
FLAGS += --verbose
endif

# Mopsa database of the build process
DB := $(realpath .)/mopsa.db

# C compiler
CC = clang


# Compilation flags of coreutils programs
CFLAGS := -O0



##################
# Analysis rules #
##################

.PHONY: prepare download configure db all ${PROGRAMS} stats clean ci

all: ${PROGRAMS}

prepare: download configure db

download: ${SRC}

${SRC}:
	@mkdir -p src
	wget https://ftp.gnu.org/gnu/coreutils/coreutils-8.30.tar.xz
	tar xaf coreutils-8.30.tar.xz -C src
	rm coreutils-8.30.tar.xz
	patch -p1 < support/mopsa.patch

configure:
	cd ${SRC} && \
	./configure CFLAGS="${CFLAGS}" CC=${CC}

db: ${DB}

${DB}:
	cd ${SRC} && \
	MOPSADB=${DB} ${MOPSA_MAKE}

.SECONDEXPANSION:
${PROGRAMS}: $$(sort $$(filter $$@%,${CASES}))


${CASES}:
	@mkdir -p ${OUTPUT}
	@support/analyze.py --output-dir ${OUTPUT} --mopsa ${MOPSA} --timeout ${TIMEOUT} ${FLAGS} $@

stats:
	@support/stats.py ${OUTPUT}

ci:
	@support/genci.py support/programs.txt > .gitlab-ci.yml

clean:
	@rm -rf ${OUTPUT}
