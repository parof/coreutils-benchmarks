#!/usr/bin/python3
##############################################################################
#                                                                            #
#  This file is part of MOPSA, a Modular Open Platform for Static Analysis.  #
#                                                                            #
#  Copyright (C) 2017-2019 The MOPSA Project.                                #
#                                                                            #
#  This program is free software: you can redistribute it and/or modify      #
#  it under the terms of the GNU Lesser General Public License as published  #
#  by the Free Software Foundation, either version 3 of the License, or      #
#  (at your option) any later version.                                       #
#                                                                            #
#  This program is distributed in the hope that it will be useful,           #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#  GNU Lesser General Public License for more details.                       #
#                                                                            #
#  You should have received a copy of the GNU Lesser General Public License  #
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.     #
#                                                                            #
##############################################################################


# Wrapper script to run Mopsa on a coreutils program

import argparse
import os
import re
import json
from common import *



# Get the command line arguments
def get_args():
    parser = argparse.ArgumentParser(description='Wrapper to run Mopsa on a Coreutils program.')
    parser.add_argument('target',
                        help="Analysis target encoded as program:scenario:abstraction")
    parser.add_argument('--mopsa',
                        help="Path to mopsa-c binary",
                        default="mopsa-c")
    parser.add_argument('--output-dir',
                        help="Path to output directory",
                        dest='output')
    parser.add_argument('--timeout',
                        help="Analysis timeout in seconds",
                        type=int,
                        dest='timeout',
                        default=3600)
    parser.add_argument('--verbose',
                        help="Display the issued mopsa-c commands",
                        action='store_true')
    return parser.parse_args()


# Get MOPSA flags common between the analysis of good and bad cases
def get_common_mopsa_flags():
    return "mopsa.db support/stubs.c \
    -format=json -no-warning -no-color -silent \
    -cell-smash \
    -loop-unrolling=0 \
    -loop-full-unrolling-at=version-etc.c:203 \
    -c-pack=@argv,@arg,%main,%getopt_long,optind,%execvp \
    -use-stub=quotearg_buffer_restyled \
    -stub-ignore-case=getopt_long.modify-argv \
    -hook c-analysis-bugs -hook c.coverage -c-show-line-coverage"


# Get MOPSA flags
def get_mopsa_flags(program,scenario,abstraction,output):
    common = get_common_mopsa_flags()
    flags = common + " -config=c/%s.json %s -make-target=%s -output=%s/%s-%s-%s.json"%(
        abstraction['config'],
        scenario['flags'],
        program,
        output,
        program,
        scenario['name'],
        abstraction['name']
    )
    output = "%s/%s-%s-%s.log"%(
        output,
        program,
        scenario['name'],
        abstraction['name']
    )
    return flags,output


# Run MOPSA
def analyze(mopsa, progam, scenario, abstraction, output, timeout):
    flags,output = get_mopsa_flags(progam, scenario, abstraction, output)
    cmd = mopsa + " " + flags
    if args.verbose: print(cmd)
    exec_shell(cmd, output, timeout)

# Parse the program, the scenario and the abstraction from the target
def parse_target(target):
    m = re.search('([^/]+)/([^/]+)/([^/]+)',target)
    program = m.group(1)
    scenario = SCENARIOS[m.group(2)]
    abstraction = ABSTRACTIONS[m.group(3)]
    return program,scenario,abstraction

# Start here
args = get_args()
program, scenario, abstraction = parse_target(args.target)
print("Analyzing '%s' with %s using abstraction %s"%(program, scenario['title'], abstraction['name']))
analyze(args.mopsa, program, scenario, abstraction, args.output, args.timeout)

# Parse the report and return an error code if the analysis isn't successful
report = "%s/%s-%s-%s.json"%(args.output,program,scenario['name'],abstraction['name'])
with open(report) as f:
    data = json.load(f)
    if data["success"]:
        exit(0)
    else:
        exit(1)
