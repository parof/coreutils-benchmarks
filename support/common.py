##############################################################################
#                                                                            #
#  This file is part of MOPSA, a Modular Open Platform for Static Analysis.  #
#                                                                            #
#  Copyright (C) 2017-2019 The MOPSA Project.                                #
#                                                                            #
#  This program is free software: you can redistribute it and/or modify      #
#  it under the terms of the GNU Lesser General Public License as published  #
#  by the Free Software Foundation, either version 3 of the License, or      #
#  (at your option) any later version.                                       #
#                                                                            #
#  This program is distributed in the hope that it will be useful,           #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#  GNU Lesser General Public License for more details.                       #
#                                                                            #
#  You should have received a copy of the GNU Lesser General Public License  #
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.     #
#                                                                            #
##############################################################################

from subprocess import Popen, PIPE
import shlex
import os
import signal


# List of scenarios
SCENARIOS = {
    "no-arg": {"title": "no argument", "name": "no-arg", "flags": "-c-symbolic-args=0"},
    "one-symbolic-arg": {"title": "one symbolic argument",   "name": "one-symbolic-arg",   "flags": "-c-symbolic-args=1"},
    "many-symbolic-args": {"title": "many symbolic arguments", "name": "many-symbolic-args", "flags": ""}
}

# List of configurations to use for every scenario
ABSTRACTIONS = {
    "a1": {"name": "a1", "config": "cell-itv-congr"},
    "a2": {"name": "a2", "config": "cell-string-length-itv-congr"},
    "a3": {"name": "a3", "config": "cell-string-length-pack-rel-itv-congr"},
    "a4":  {"name": "a4", "config": "cell-string-length-pointer-sentinel-pack-rel-itv-congr"}
}

# Execute a command in the shell
def exec_shell(cmd, output, timeout):
    with open(output, 'w') as output:
        proc = Popen(shlex.split(cmd), stdout=output)
        try:
            proc.communicate(timeout=timeout)
        except:
            os.killpg(os.getpgid(proc.pid), signal.SIGKILL)
